#
# Lineage Audio Files
#

ALARM_PATH := vendor/rr/prebuilt/common/media/audio/alarms
NOTIFICATION_PATH := vendor/rr/prebuilt/common/media/audio/notifications
RINGTONE_PATH := vendor/rr/prebuilt/common/media/audio/ringtones

# Alarms
PRODUCT_COPY_FILES += \
    $(ALARM_PATH)/A_real_hoot.ogg:system/media/audio/alarms/A_real_hoot.ogg \
    $(ALARM_PATH)/Awaken.ogg:system/media/audio/alarms/Awaken.ogg \
    $(ALARM_PATH)/Bounce.ogg:system/media/audio/alarms/Bounce.ogg \
    $(ALARM_PATH)/Bright_morning.ogg:system/media/audio/alarms/Bright_morning.ogg \
    $(ALARM_PATH)/Connect.mp3:system/media/audio/alarms/Connect.mp3 \
    $(ALARM_PATH)/Cuckoo_clock.ogg:system/media/audio/alarms/Cuckoo_clock.ogg \
    $(ALARM_PATH)/Drip.ogg:system/media/audio/alarms/A_real_Drip.ogg \
    $(ALARM_PATH)/Early_twilight.ogg:system/media/audio/alarms/Early_twilight.ogg \
    $(ALARM_PATH)/Flow.ogg:system/media/audio/alarms/Flow.ogg \
    $(ALARM_PATH)/Full_of_wonder.ogg:system/media/audio/alarms/Full_of_wonder.ogg \
    $(ALARM_PATH)/Gallop.ogg:system/media/audio/alarms/Gallop.ogg \
    $(ALARM_PATH)/Gentle_breeze.ogg:system/media/audio/alarms/Gentle_breeze.ogg \
    $(ALARM_PATH)/Icicles.ogg:system/media/audio/alarms/Icicles.ogg \
    $(ALARM_PATH)/Jump_start.ogg:system/media/audio/alarms/Jump_start.ogg \
    $(ALARM_PATH)/Loose_change.ogg:system/media/audio/alarms/Loose_change.ogg \
    $(ALARM_PATH)/Nudge.ogg:system/media/audio/alarms/Nudge.ogg \
    $(ALARM_PATH)/Orbit.ogg:system/media/audio/alarms/Orbit.ogg \
    $(ALARM_PATH)/Rise.ogg:system/media/audio/alarms/Rise.ogg \
    $(ALARM_PATH)/Rolling_fog.ogg:system/media/audio/alarms/Rolling_fog.ogg \
    $(ALARM_PATH)/Spokes.ogg:system/media/audio/alarms/Spokes.ogg \
    $(ALARM_PATH)/Sunshower.ogg:system/media/audio/alarms/Sunshower.ogg \
    $(ALARM_PATH)/Sway.ogg:system/media/audio/alarms/Sway.ogg \
    $(ALARM_PATH)/Wag.ogg:system/media/audio/alarms/Wag.ogg

# Notifications
PRODUCT_COPY_FILES += \
    $(NOTIFICATION_PATH)/Beginning.ogg:system/media/audio/notifications/Beginning.ogg \
    $(NOTIFICATION_PATH)/Birdsong.ogg:system/media/audio/notifications/Birdsong.ogg \
    $(NOTIFICATION_PATH)/Chime.ogg:system/media/audio/notifications/Chime.ogg \
    $(NOTIFICATION_PATH)/Clink.ogg:system/media/audio/notifications/Clink.ogg \
    $(NOTIFICATION_PATH)/Coconuts.ogg:system/media/audio/notifications/Coconuts.ogg \
    $(NOTIFICATION_PATH)/Duet.ogg:system/media/audio/notifications/Duet.ogg \
    $(NOTIFICATION_PATH)/End_note.ogg:system/media/audio/notifications/End_note.ogg \
    $(NOTIFICATION_PATH)/Flick.ogg:system/media/audio/notifications/Flick.ogg \
    $(NOTIFICATION_PATH)/Gentle_gong.ogg:system/media/audio/notifications/Gentle_gong.ogg \
    $(NOTIFICATION_PATH)/Hey.ogg:system/media/audio/notifications/Hey.ogg \
    $(NOTIFICATION_PATH)/Mallet.ogg:system/media/audio/notifications/Mallet.ogg \
    $(NOTIFICATION_PATH)/Note.ogg:system/media/audio/notifications/Note.ogg \
    $(NOTIFICATION_PATH)/Orders_up.ogg:system/media/audio/notifications/Orders_up.ogg \
    $(NOTIFICATION_PATH)/Ping.ogg:system/media/audio/notifications/Ping.ogg \
    $(NOTIFICATION_PATH)/Pipes.ogg:system/media/audio/notifications/Pipes.ogg \
    $(NOTIFICATION_PATH)/Popcorn.ogg:system/media/audio/notifications/Popcorn.ogg \
    $(NOTIFICATION_PATH)/Shopkeeper.ogg:system/media/audio/notifications/Shopkeeper.ogg \
    $(NOTIFICATION_PATH)/Sticks_and_stones.ogg:system/media/audio/notifications/Sticks_and_stones.ogg \
    $(NOTIFICATION_PATH)/Strum.ogg:system/media/audio/notifications/Strum.ogg \
    $(NOTIFICATION_PATH)/Trill.ogg:system/media/audio/notifications/Trill.ogg \
    $(NOTIFICATION_PATH)/Tuneup.ogg:system/media/audio/notifications/Tuneup.ogg \
    $(NOTIFICATION_PATH)/Tweeter.ogg:system/media/audio/notifications/Tweeter.ogg \
    $(NOTIFICATION_PATH)/Twinkle.ogg:system/media/audio/notifications/Twinkle.ogg

# Ringtones
PRODUCT_COPY_FILES += \
	$(RINGTONE_PATH)/Arabia.mp3:system/media/audio/ringtones/Arabia.mp3 \
	$(RINGTONE_PATH)/Arctic.mp3:system/media/audio/ringtones/Arctic.mp3 \
	$(RINGTONE_PATH)/Beats.ogg:system/media/audio/ringtones/Beats.ogg \
	$(RINGTONE_PATH)/Copycat.ogg:system/media/audio/ringtones/Copycat.ogg \
	$(RINGTONE_PATH)/Crackle.ogg:system/media/audio/ringtones/Crackle.ogg \
	$(RINGTONE_PATH)/Dance_party.ogg:system/media/audio/ringtones/Dance_party.ogg \
	$(RINGTONE_PATH)/Early_bird.ogg:system/media/audio/ringtones/Early_bird.ogg \
	$(RINGTONE_PATH)/Flutterby.ogg:system/media/audio/ringtones/Flutterby.ogg \
	$(RINGTONE_PATH)/Hey_hey.ogg:system/media/audio/ringtones/Hey_hey.ogg \
	$(RINGTONE_PATH)/Hotline.ogg:system/media/audio/ringtones/Hotline.ogg \
	$(RINGTONE_PATH)/Leaps_and_bounds.ogg:system/media/audio/ringtones/Leaps_and_bounds.ogg \
	$(RINGTONE_PATH)/Lollipop.ogg:system/media/audio/ringtones/Lollipop.ogg \
	$(RINGTONE_PATH)/Lost_and_found.ogg:system/media/audio/ringtones/Lost_and_found.ogg \
	$(RINGTONE_PATH)/Mash_up.ogg:system/media/audio/ringtones/Mash_up.ogg \
	$(RINGTONE_PATH)/Monkey_around.ogg:system/media/audio/ringtones/Monkey_around.ogg \
	$(RINGTONE_PATH)/Romance.ogg:system/media/audio/ringtones/Romance.ogg \
	$(RINGTONE_PATH)/Rose.ogg:system/media/audio/ringtones/Rose.ogg \
	$(RINGTONE_PATH)/Rrrring.ogg:system/media/audio/ringtones/Rrrring.ogg \
	$(RINGTONE_PATH)/Schools_out.ogg:system/media/audio/ringtones/Schools_out.ogg \
	$(RINGTONE_PATH)/Sea.ogg:system/media/audio/ringtones/Sea.ogg \
	$(RINGTONE_PATH)/Shooting_star.ogg:system/media/audio/ringtones/Shooting_star.ogg \
	$(RINGTONE_PATH)/Spaceship.ogg:system/media/audio/ringtones/Spaceship.ogg \
	$(RINGTONE_PATH)/Summer_night.ogg:system/media/audio/ringtones/Summer_night.ogg \
	$(RINGTONE_PATH)/The_big_adventure.ogg:system/media/audio/ringtones/The_big_adventure.ogg \
	$(RINGTONE_PATH)/Zen.ogg:system/media/audio/ringtones/Zen.ogg \
	$(RINGTONE_PATH)/Zen_too.ogg:system/media/audio/ringtones/Zen_too.ogg \
	$(RINGTONE_PATH)/iOS.ogg:system/media/audio/ringtones/.ogg 
